# デモ手順

0. 事前の物理配線
 - swp1 - swp2 (VLAN100同士の通信デモ用)
 - (これはやりません)　swp3 - swp21 (VLAN100->VLAN101の通信デモ用)

1. Cumulusを再起動
sudo reload

2. 立ち上がる瞬間にEnter key

3. onieを立ち上げる
run onie_bootcmd

4. しばらく待つ

5. ansibleコマンドが自動で実施される (かっきーさんPCで実施)
 - 手動でやる場合は下記コマンドで実施
   - cd ./wbs
   - ansible-playbook site.yml -i hosts

6. ansibleが成功すると、このような構成になる
 - swp1
   - 192.168.10.1/24
   - untag VLAN 100
 - swp2
   - 192.168.10.2/24
   - untag VLAN 100
 - swp3
   - 192.168.10.2/24
   - untag VLAN 100
 - swp21
   - 192.168.11.1/24
   - untag VLAN 101
 - swp22
   - 192.168.11.2/24
   - untag VLAN 101

7. pingで疎通を確認（Cumulusで実施）
   - swp1 -> swp 2
     - ping 192.168.10.2 -I 192.168.10.1  
       - ping通る
     - VLAN 100 同士の通信なのでpingは成功

   - (これはやりません。) swp3 -> swp21
     - ping 192.168.11.1 -I 192.168.10.3 
       - あれ、ping通るじゃん。。
     - VLAN 100 -> VLAN 101 の通信なのでPingは失敗
     
