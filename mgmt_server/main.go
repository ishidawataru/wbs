package main

import (
	"bytes"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/jessevdk/go-flags"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"os/exec"
	"strconv"
	"strings"
)

var globalOpts struct {
	Port    int    `short:"p" long:"port" description:"specifying a port" default:"8080"`
	WorkDir string `short:"d" long:"dir" description:"specifying work dir" default:"."`
}

type RestServer struct {
	port int
}

func NewRestServer(port int) *RestServer {
	rs := &RestServer{
		port: port,
	}
	return rs
}

func (rs *RestServer) Serve() {
	r := mux.NewRouter()
	r.HandleFunc("/key", rs.NewKey).Methods("POST")
	r.HandleFunc("/host", rs.NewHost).Methods("POST")

	r.NotFoundHandler = http.HandlerFunc(NotFoundHandler)
	http.Handle("/", r)

	http.ListenAndServe(":"+strconv.Itoa(rs.port), nil)
}

type AnsibleSite struct {
	name  string
	hosts string
}

func (rs *RestServer) NewKey(w http.ResponseWriter, r *http.Request) {
	var buf bytes.Buffer
	buf.ReadFrom(r.Body)

	fmt.Println("==== NEW KEY! ====")

	fmt.Println(buf.String())

	err := ioutil.WriteFile(fmt.Sprintf("%s/id_rsa", globalOpts.WorkDir), buf.Bytes(), 0600)
	if err != nil {
		fmt.Println(err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (rs *RestServer) NewHost(w http.ResponseWriter, r *http.Request) {
	var buf bytes.Buffer
	buf.ReadFrom(r.Body)

	fmt.Println("==== ACCESS FROM NEW SWITCH ! ====")

	query, _ := url.ParseQuery(buf.String())

	if _, ok := query["ipaddr"]; !ok {
		fmt.Println("ipaddr is missing")
		http.Error(w, "ipaddr is missing", http.StatusInternalServerError)
		return
	}

	if _, ok := query["role"]; !ok {
		fmt.Println("role is missing")
		http.Error(w, "role is missing", http.StatusInternalServerError)
		return
	}

	fmt.Println("- User: cumulus")
	fmt.Println("- Pass: CumulusLinux!")
	fmt.Println("- IP: ", query["ipaddr"][0])
	fmt.Println("- role: ", query["role"][0])

	host := []byte(fmt.Sprintf("%s ansible_ssh_user=cumulus ansible_ssh_private_key_file=./id_rsa", strings.Split(query["ipaddr"][0], "/")[0]))
	role := query["role"][0]

	err := ioutil.WriteFile(fmt.Sprintf("%s/hosts", globalOpts.WorkDir), []byte(fmt.Sprintf("%s\n", host)), 0644)
	if err != nil {
		fmt.Println(err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	var buffer bytes.Buffer
	buffer.WriteString(fmt.Sprintf("- name: Ansible Auto Provisioning for %s\n", host))
	buffer.WriteString("  hosts: all\n")
	buffer.WriteString("  roles:\n")
	buffer.WriteString(fmt.Sprintf("  - %s\n", role))

	err = ioutil.WriteFile(fmt.Sprintf("%s/site.yml", globalOpts.WorkDir), buffer.Bytes(), 0644)
	if err != nil {
		fmt.Println(err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	_, err = exec.LookPath("ansible-playbook")
	if err != nil {
		fmt.Println("installing ansible-playbook is in your future")
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	cmd := exec.Command("rm", "/Users/wataru/.ssh/known_hosts")
	err = cmd.Run()
	if err != nil {
		fmt.Println(err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	cmd = exec.Command("ansible-playbook", fmt.Sprintf("%s/site.yml", globalOpts.WorkDir), "-i", fmt.Sprintf("%s/hosts", globalOpts.WorkDir), "-M", "/home/kazuki/.go/src/bitbucket.org/ishidawataru/wbs/roles/cumulus.CumulusLinux/library/")
	err = cmd.Run()
	if err != nil {
		fmt.Println(err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

}

func NotFoundHandler(w http.ResponseWriter, r *http.Request) {
	http.Error(w, http.StatusText(http.StatusNotFound), http.StatusNotFound)
}

func main() {

	_, err := flags.Parse(&globalOpts)
	if err != nil {
		os.Exit(1)
	}

	server := NewRestServer(8080)
	server.Serve()
}
